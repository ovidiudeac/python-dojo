def sample_function(x):
    """
    >>> sample_function(1)
    2
    """
    return x * 2